package com.example.practice;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import ask.scanninglibrary.ASKActivity;

public class SecondActivity extends ASKActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_second, menu);
		return true;
	}
	
	public void Back(View view) {
		finish();
	}

}
