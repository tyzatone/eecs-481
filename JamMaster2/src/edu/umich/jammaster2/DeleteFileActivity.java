package edu.umich.jammaster2;


import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import ask.scanninglibrary.ASKActivity;
import ask.scanninglibrary.views.ASKAlertDialog;

public class DeleteFileActivity extends ASKActivity 
{
	private ListAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delete_file);
		
		ListView mListView = (ListView) this.findViewById(R.id.listView1);
		mAdapter = new ListAdapter(this, mListView);
		mListView.setAdapter(mAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.delete_file, menu);
		return true;
	}

	public void Delete(View view)
	{
		String sel = mAdapter.getSelected();
		
		if (sel == null)
		{
			ASKAlertDialog.Builder dlgAlert  = new ASKAlertDialog.Builder(this);                      
		    dlgAlert.setMessage("You must selected a file to delete first.");
		    dlgAlert.setTitle("Selection Error");              
		    dlgAlert.setPositiveButton("OK", null);
		    dlgAlert.setCancelable(true);
		    dlgAlert.create().show();
		    return;
		}
		
		if (!MainActivity.self().getBaseContext().deleteFile(sel))
		{
			ASKAlertDialog.Builder dlgAlert  = new ASKAlertDialog.Builder(this);                      
		    dlgAlert.setMessage("Could not delete file " + sel);
		    dlgAlert.setTitle("File Error");              
		    dlgAlert.setPositiveButton("OK", null);
		    dlgAlert.setCancelable(true);
		    dlgAlert.create().show();
		}
		
		mAdapter.remove(mAdapter.getSelected());
		mAdapter.notifyDataSetChanged();
	}
	
	public void Back(View view)
	{
		finish();
	}
}
