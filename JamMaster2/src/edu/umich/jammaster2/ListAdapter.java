package edu.umich.jammaster2;

import java.io.File;

import android.content.Context;
import android.graphics.LightingColorFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import ask.scanninglibrary.ASKActivity;
import ask.scanninglibrary.views.ASKAdapter;

// Opens up internal folder the activity is part of and looks for all the Song files and populates a listview with buttons
// that have text with the Song file name.

public class ListAdapter extends ASKAdapter<String>
{
	private String selected = null;
	private View last;
	
	private ListAdapter(Context context) 
	{
		super(context, 0);
	}
	
	public ListAdapter(ASKActivity context, ListView listView) 
	{
		super(context, R.layout.button_cell, listView);
		
		File dirFile = MainActivity.self().getFilesDir();
		Log.v("MsgFilter", "Files: " + dirFile.list().length);
		
		for (String strFile : dirFile.list())
			if (strFile.endsWith(".song"))
				add(strFile);
		
		notifyDataSetChanged();
	}

	@Override
	public View askGetView(final int position, View convertView, ViewGroup parent) 
	{
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout buttonView = (RelativeLayout) inflater.inflate(R.layout.button_cell, parent, false);
		Button button = (Button) buttonView.findViewById(R.id.buttonA);
		
		button.setText(getItem(position));
		
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if (last != null)
				{
					last.getBackground().setColorFilter(null);
					((Button)last).setTextColor(0xFF000000);
				}
				
				view.getBackground().setColorFilter(new LightingColorFilter(0x00000000, 0x000076C6));// 0x000000FF));
				((Button)view).setTextColor(0xFFFFFFFF);
				
				selected = getItem(position);
				last = view;
			}
		});
		
		return buttonView;
	}
	
	public String getSelected()
	{
		return selected;
	}
}
