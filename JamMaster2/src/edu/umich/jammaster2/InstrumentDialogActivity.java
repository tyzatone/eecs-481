package edu.umich.jammaster2;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import ask.scanninglibrary.ASKActivity;

public class InstrumentDialogActivity extends ASKActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_instrument_dialog);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.instrument_dialog, menu);
		return true;
	}
	
	public void Select(View view)
	{
		int type = -1;
		
		if (view == findViewById(R.id.button1))
			type = 0;
		
		if (view == findViewById(R.id.button2))
			type = 1; 
		
		if (view == findViewById(R.id.button3))
			type = 2;
		
		Intent result = new Intent();
		result.putExtra("instrument", type);
		setResult(Activity.RESULT_OK, result);
		finish();
	}
	
	public void Back(View view)
	{
		setResult(Activity.RESULT_CANCELED, getIntent());
		finish();
	}
}
