package edu.umich.jammaster2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;

import android.content.Context;
import android.util.Log;

/*
 * - must have at least one sound in it. 
 */
public class SoundResIDArray 
{
	private static final String fileExtension = ".sa";
	
	private int[] ids;
	private String name;
	
	/* 
	 * - makes a default major-scale piano array. 
	 */
	public SoundResIDArray(int type) throws FileNotFoundException, IOException 
	{
		Log.v("MsgFilter", "Setting instrument type - " + type);
		
		switch (type)
		{
		case 0:
			ids = new int[] 
			{
				R.raw.piano_g,
				R.raw.piano_f,
				R.raw.piano_e,
				R.raw.piano_d,
				R.raw.piano_c,
				R.raw.piano_b,
				R.raw.piano_a, 
			};
			
			name = "piano";
			save();
			break;
			
		case 1:
			ids = new int[] 
			{
				 R.raw.drum_bass,
				 R.raw.drum_clap,
				 R.raw.drum_groove,
				 R.raw.drum_hihat,
				 R.raw.drum_percusion,
				 R.raw.drum_rimshot,
				 R.raw.drum_snare,
			};
			
			name = "drumkit";
			save();
			break;
		
		case 2:
			ids = new int[] 
			{
				R.raw.trumpet_g2,
				R.raw.trumpet_f2,
				R.raw.trumpet_e2,
				R.raw.trumpet_d,
				R.raw.trumpet_c,
				R.raw.trumpet_b,
				R.raw.trumpet_a,
			};
			
			name = "trumpet";
			save();
			break;
			
		default:
			Log.v("MsgFilter", "UNRECOGNIZED");
			break;
		}
	}
	
	/*
	 * - 'fileName' should not include any suffix. 
	 * - 'fileName' should be the name of a SoundArray file that has been saved by 'save()'. 
	 */
	public SoundResIDArray(String fileName) throws NumberFormatException, IOException 
	{
		Log.v("MsgFilter", "Opening instrument - " + fileName);
		
		name = fileName;
		FileInputStream fStream = MainActivity.self().openFileInput(fileName + fileExtension);
		BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
		int numSounds = Integer.valueOf(br.readLine());
		ids = new int[numSounds];
		
		for (int i = 0; i < numSounds; ++i) 
		{
			ids[i] = Integer.valueOf(br.readLine());
		}
		
		assert(ids.length >= 1);
		Log.v("MsgFilter", "Opened");
	}
	
	public String getName() { return name; }
	
	public int getSize() { return ids.length; }
	
	public String getName(int index) 
	{
		return MainActivity.self().getResources().getResourceName(ids[index]);
	}
	
	public int getResID(int index) 
	{
		assert (index < ids.length);
		return ids[index];
	}
	
	public void setSound(int resID, int index) throws FileNotFoundException, IOException 
	{
			ids[index] = resID;
	}
	
	public void setName(String newName) 
	{
		name = newName;
	}
	
	/*
	 * - saves this SoundArray to a file named "[name].[fileExtension]". 
	 * - The file contains the resource IDs of each Sound, in order, one per line. 
	 * - The first line of the file contains the number of resIDs in the file. 
	 */
	public void save() throws IOException
	{
		FileOutputStream fStream = 
				MainActivity.self().openFileOutput(name + fileExtension, Context.MODE_PRIVATE);
		
		OutputStreamWriter osw = new OutputStreamWriter(fStream);
		String numSoundsStr = new String(String.valueOf(ids.length));
		osw.write(numSoundsStr + "\n");
		
		for (int i = 0; i < ids.length; ++i) 
		{
			osw.write(String.valueOf(ids[i]) + "\n");
		}
		
		osw.close();
	}
}
