package edu.umich.jammaster2;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow;
import ask.scanninglibrary.*;

import java.util.Timer;
import java.util.TimerTask;

public class JAMScanningMethod extends ScanningMethod {
	
	private Timer mTimer;
	private boolean accepting;
	private int mRow;
	
	/**
	 * The time delay between scanning.
	 */
	private static int wait_time_ms = 1000;
	
	public Boolean mReady = false; 
	
	public JAMScanningMethod(Context ctx) {
		super(ctx);
		accepting = false;
		wait_time_ms = Settings.getScanningSpeed(ctx);
		mRow = 1;
	}
	
	/**
	 * Sets up the current screenoverlay with an onclick listener that 
	 * waits for input, and scans over rows/views until one is selected.
	 * At this point the programmer should either ask the interface to 
	 * accept input again, or move on.
	 */

	public void acceptInput(final Scannable scannableView) {
		Log("acceptInput " + accepting);
		

		if (accepting) {
			stopAcceptingInput();
		}
		accepting = true;
		
		mScreenOverlay.post(new Runnable(){
			@Override
			public void run() {
				mPopup.showAtLocation(scannableView.getRootView(), 0, 0, 0);	
				mPopup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
			}
		});
		
		class task extends TimerTask {
			int index = -1; //Otherwise first iteration would be 1
			
			boolean rowLock = false;
			
			OnClickListener STAGE_1 = new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					rowLock = true;
					mScreenOverlay.setOnClickListener(null);
					if(scannableView.numViewsForRow(mRow) == 1) {
						selectView(mRow, 0);
					} else {
						scannableView.selectRow(mScreenOverlay, mRow);
					}
				}
			};
			
			OnClickListener STAGE_2 = new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					
					selectView(mRow, index);
					
					mScreenOverlay.setOnClickListener(STAGE_1);
					rowLock = false;
					mRow++;
				}
			};
			
			void selectView(final int row, final int index) {
				if (scannableView.selectView(mScreenOverlay, row, index))
				{	 
					this.index = -1;
					rowLock = false;
				} else {
					JAMScanningMethod.this.stopAcceptingInput();
				}
			}
			
			@Override
			public void run() {
				new Handler(Looper.getMainLooper()).post(new Runnable(){
					
					@Override
					public void run() {
						mScreenOverlay.clearHighlights();
						mPopup.showAtLocation(scannableView.getRootView(), 0, 0, 0);	
						mPopup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
						if(rowLock) {
							if(index == -1) mScreenOverlay.setOnClickListener(STAGE_2);
							if(++index == scannableView.numViewsForRow(mRow)) index = 0;
							scannableView.highlightView(mScreenOverlay, mRow, index);
						} else {
							if(index == -1) {
								mScreenOverlay.setOnClickListener(STAGE_1);
							}
							if(--mRow == -1) mRow = scannableView.numRows() - 1;
							scannableView.highlightRow(mScreenOverlay, mRow);
						}
						mPopup.update();
					}
				});
			}
		}
		
		mTimer = new Timer();
		mTimer.scheduleAtFixedRate(new task(), wait_time_ms, wait_time_ms);
	}

	/**
	 * Cancels the timer so it will stop scanning.
	 */
	public void stopAcceptingInput() {
		Log("stopAcceptingInput");
		
		if(mTimer != null) {
			mTimer.cancel();
			mTimer.purge();
			mTimer = null;
		}
		
		mScreenOverlay.removeAllViews();
		mPopup.dismiss();
		accepting = false;
	}
	
	private static void Log(String msg) {
		Log.wtf("ScanningMethod", msg);
	}

	@Override
	public boolean isAcceptingInput() {
		return (mTimer != null);
	}

}
