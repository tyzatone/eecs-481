package edu.umich.jammaster2;

import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;

/*
 * ***All documentation that applies to multiple classes in this package goes here. ***
 * 
 * - Each row is a different note; each column is a different time. Lower row indices 
 * 		are near the bottom. Lower column indices are to  
 * 		the left. 
 * - The row that has the play button, tempo buttons, etc. is referred to as the 
 * 		control button row. 
 * - Player has been put in a package because it has a SongInterface, and other 
 * 		classes may end up having their own SongInterface classes. The packages 
 * 		allow classes to refer to SongInterface classes without ambiguity. In the 
 * 		long run, this setup is less verbose and more organized. 
 */

public class MainActivity extends JAMActivity {
	
	public final String DEBUG_TAG = new String("MainActivity");
	
	private final int PICK_INSTRUMENT = 1;
	private final int SONG_SELECTION  = 2;
	
	// These make it easy to do Sound stuff. 
	private static MainActivity self;
	public static MainActivity self() { return self; }
	
	private Drawable playOn, playOff;
	private boolean playing = false;
	private ImageButton play;
	
	GridView mGridView;
	Song song;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		self = this;
		
		try { song = new Song(this); } 
		catch (FileNotFoundException e) { e.printStackTrace(); } 
		catch (IOException e) { e.printStackTrace(); }
		
		initControlButtonRow(song.getNumRows());
		
		song.setNumColumns(calcNumGridColumns(song.getNumRows()));
		mGridView = (GridView) this.findViewById(R.id.gridView1);
		song.setAdapterToGrid(mGridView);
		
		MixerPlayer.getInstance().setSong(song);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void Play(View view)
	{
		playing = !playing;
		
		if (playing)
		{
			play.setImageDrawable(playOn);
			MixerPlayer.getInstance().play();
		}
		
		else
		{
			play.setImageDrawable(playOff);
			MixerPlayer.getInstance().stop();
		}
	}
	
	public void TempoDown(View view)
	{
		song.decreaseTempo();
	}
	
	public void TempoUp(View view)
	{
		song.increaseTempo();
	}
	
	public void InstrumentDialog(View view)
	{
		Intent instrumentDialog = new Intent(this, InstrumentDialogActivity.class);
		startActivityForResult(instrumentDialog, PICK_INSTRUMENT);
	}
	
	public void Clear(View view)
	{
		song.clearGrid();
	}
	
	public void FileDialog(View view)
	{
		Intent fileDialog = new Intent(this, FileDialogActivity.class);
		fileDialog.putExtra("song", song);
		startActivityForResult(fileDialog, SONG_SELECTION);
	}
	
	// When you return to this activity, grab the necessary extra data from
	// the previous activity (FileDialog).
	@Override 
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{     
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == RESULT_CANCELED) return;
		
		switch(requestCode)
		{
		case SONG_SELECTION:
			if (!data.hasExtra("song")) return;
			song = data.getParcelableExtra("song");
			song.setAdapterToGrid(mGridView);
			MixerPlayer.getInstance().setSong(song);
			break;
			
		case PICK_INSTRUMENT:
			if (!data.hasExtra("instrument")) return;
			int type = data.getExtras().getInt("instrument");
			try { song.setSoundResIDArray(type); }
			catch (Exception e) { Log.v("MsgFilter", "Could not open instrument."); }
			break;
			 
		default:
			Log.v("MsgFilter", "ERROR: Invalid request code.");
			break;
		}
	}
	
	public void Exit(View view)
	{
		play.setImageDrawable(playOff);
		MixerPlayer.getInstance().stop();
		
		finish();
	}
	
	
	/* Private Functions */
	
	private void initControlButtonRow(int numRows) 
	{
		play = (ImageButton) findViewById(R.id.imageButton1);
		
		LinearLayout controlRow = (LinearLayout) findViewById(R.id.linearLayout1);
		
		LinearLayout.LayoutParams params = 
			new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		int cellSize = getCellSize(numRows);
		params.width = cellSize;
		params.height = cellSize;
		params.rightMargin = 4;
		//params.leftMargin = 1;
		
		for (int i = 0; i < controlRow.getChildCount(); i++)
		{
		      View v = controlRow.getChildAt(i);
		      v.setLayoutParams(params);
		}
			
		playOn = getResources().getDrawable(R.drawable.play_on);
		playOff = getResources().getDrawable(R.drawable.play_off);
	}
	
	private int calcNumGridColumns(int numRows) 
	{
		Display display = getWindowManager().getDefaultDisplay();
		Point point = new Point();
		display.getSize(point);

		int height = point.y;
		
		int result;
		int cellSize = getCellSize(numRows);
		for (result = 0; result < (height - (cellSize + 4) * 1.5) / (cellSize + 4); ++result);
		
		return result;
	}
	
	private int getCellSize(int numRows) 
	{
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;

		int hSpace = 4;
		
	    return ((width - 20) + hSpace) / numRows - (hSpace * 2);
	}
}

