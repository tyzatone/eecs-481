package edu.umich.jammaster2;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import ask.scanninglibrary.ASKActivity;
import ask.scanninglibrary.views.ASKAlertDialog;
import android.util.Log;

public class LoadFileActivity extends ASKActivity 
{
	private ListAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_file);
		
		ListView mListView = (ListView) this.findViewById(R.id.listView1);
		mAdapter = new ListAdapter(this, mListView);
		mListView.setAdapter(mAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.load_file, menu);
		return true;
	}

	public void Open(View view)
	{
		String sel = mAdapter.getSelected();
		
		if (sel == null)
		{
			ASKAlertDialog.Builder dlgAlert  = new ASKAlertDialog.Builder(this);                      
		    dlgAlert.setMessage("You must selected a file to load first.");
		    dlgAlert.setTitle("Selection Error");              
		    dlgAlert.setPositiveButton("OK", null);
		    dlgAlert.setCancelable(true);
		    dlgAlert.create().show();
		    return;
		}
		
		try 
		{ 
			Intent result = new Intent();
			Song song = new Song(this, sel);
			result.putExtra("song", song);
			setResult(Activity.RESULT_OK, result);
			finish();
		}
		
		catch (Exception e)
		{
			ASKAlertDialog.Builder dlgAlert  = new ASKAlertDialog.Builder(this);                      
		    dlgAlert.setMessage("Could not read file " + sel);
		    dlgAlert.setTitle("File Error");              
		    dlgAlert.setPositiveButton("OK", null);
		    dlgAlert.setCancelable(true);
		    dlgAlert.create().show();
		    
		    Log.v("MsgFilter", e.getMessage());
		}
	}
	
	public void Back(View view)
	{
		setResult(Activity.RESULT_CANCELED, null);
		finish();
	}
}
