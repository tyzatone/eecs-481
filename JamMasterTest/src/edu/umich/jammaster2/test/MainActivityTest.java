package edu.umich.jammaster2.test;

import java.io.FileNotFoundException;
import java.io.IOException;

import android.test.ActivityInstrumentationTestCase2;
import edu.umich.jammaster2.*;

public class MainActivityTest extends
		ActivityInstrumentationTestCase2<MainActivity> {
	
	private MainActivity activity;
	
	public MainActivityTest() {
		super(MainActivity.class);
	}
	
	public void testSoundArray1() throws FileNotFoundException, IOException, InterruptedException {
		SoundArray sa = new SoundArray();
		sa.save();
		SoundArray loadedSoundArray = new SoundArray(sa.getName());
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < loadedSoundArray.getSize(); ++j) {
				loadedSoundArray.play(j);
				Thread.sleep(500);
			}
		}
	}
	
	public void setUp() throws Exception {
		super.setUp();
		
		activity = getActivity();
	}
}
